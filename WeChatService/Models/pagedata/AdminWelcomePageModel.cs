﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BaoShengYuan.Model.pagedata
{
    public class AdminWelcomePageModel
    {
        public int chartyear { get; set; }         //统计年份
        public int userall { get; set; }           //游客注册总数
        public int userpicall { get; set; }        //游客图片总数
        public int ticketorderall { get; set; }    //票务订单总数
        public int ticketorderyuding { get; set; } //订票订单总数
        public int ticketordergoumai { get; set; } //购票订单总数

        public int proorderall { get; set; }       //商城订单总数
        public int waitbackmoney { get; set; }
        

        public AdminWelcomePageModel()
        {
        }
    }
}