﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WeChatService.Models
{
    public class Common
    {
        public static int backPageSize = 15;//后台页面 列表数量

        public static String HTTP = "http://";
        public static String ContentUrl = "Content/index?p=";
        public static String CoverImageUrl_Big = "Upload/CoverImage/";
        public static String CoverImageUrl_Small = "Upload/CoverImage/S400/";
        public static String DefaultGroupName = "All";

        public static String SuccessMsg = "操作成功";
        public static String FailedMsg = "操作失败";
        public static String ExistMsg = "记录已经存在，操作失败";
        public static String Error_1 = "非多图文类型，不允许多条消息";
        public static String MenuCreateError_1 = "菜单不可为空";
        public static String MenuCreateSuccess = "菜单生成成功";
        public static String MenuDeleteSuccess = "菜单禁用成功";

        public static int EventType_Follow = 1;
        public static int EventType_Key = 2;
        public static int EventType_Other = 3;
        public static int EventType_UnFollow = -1;

        public static int MsgType_Txt = 1;
        public static int MsgType_Music = 2;
        public static int MsgType_PictureTxt = 3;
        public static int MsgType_PictureTxts = 4;

        public static String CustomMenuCreateErrorMsg_1 = "根菜单数量最多3个";
        public static String CustomMenuCreateErrorMsg_2 = "同一个根菜单，子菜单数量最多5个";
        
        public static String loginpage = System.Web.Configuration.WebConfigurationManager.AppSettings["loginpage"];
        public static String appId = System.Web.Configuration.WebConfigurationManager.AppSettings["appId"];
        public static String appSecret = System.Web.Configuration.WebConfigurationManager.AppSettings["appSecret"];
        public static String token = System.Web.Configuration.WebConfigurationManager.AppSettings["token"];
        public static String wxName = System.Web.Configuration.WebConfigurationManager.AppSettings["wxName"];
        public static String thisurl = System.Web.Configuration.WebConfigurationManager.AppSettings["thisurl"];
    
    }
}