﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WeChatService.Models
{
    public interface IServiceBase<T> where T : class
    {
        List<T> GetAll();
        int Add(T entity);
        int Del(T entity);
        int DelList(List<T> list);
    }
}
