﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WeChatService.Models.efmodel;

namespace WeChatService.Models
{
    public class DataContextManager
    {
        [ThreadStatic]
        private static wechatEntities dataContext = null;
        /// <summary>
        /// 获取DataContext对象
        /// </summary>
        /// <returns></returns>
        public static wechatEntities GetContext()
        {
            if (dataContext == null)
                dataContext = new wechatEntities();
            return dataContext;
        }

        /// <summary>
        /// 重置DataContext对象
        /// </summary>
        public static void Clear()
        {
            dataContext = null;
        }
    }
}