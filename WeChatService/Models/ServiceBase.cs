﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WeChatService.Models.efmodel;
using WeChatService.Models;

namespace WeChatService.Models{
    public abstract class ServiceBase<T> : IServiceBase<T> where T : class
    {
        public wechatEntities we = DataContextManager.GetContext();

        public List<T> GetAll()
        {

            return we.CreateObjectSet<T>().ToList();
        }

        public int Add(T entity)
        {
            we.CreateObjectSet<T>().AddObject(entity);
            return we.SaveChanges();
        }

        public int Del(T entity)
        {
            we.CreateObjectSet<T>().DeleteObject(entity);
            return we.SaveChanges();
        }

        public int DelList(List<T> entity)
        {
            foreach (var item in entity)
            {
                we.CreateObjectSet<T>().DeleteObject(item);
            }
            return we.SaveChanges();
        }


    }
}