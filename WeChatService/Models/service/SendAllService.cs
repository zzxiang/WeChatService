﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WeChatService.Models.efmodel;
using Webdiyer.WebControls.Mvc;

namespace WeChatService.Models.service
{
    public class SendAllService : ServiceBase<SendAll>
    {
        public PagedList<SendAll> getPage(int pi, int pz)
        {
            return we.SendAll.OrderByDescending(j => j.ID).ToPagedList(pi, pz);
        }
    }
}