﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WeChatService.Models.efmodel;

namespace WeChatService.Models.service
{
    public class AdminService : ServiceBase<Admin>
    {

        private Admin getAdminByAname(String aname)
        {
            return we.Admin.SingleOrDefault(a => a.Aname == aname);
        }

        public int changePass(string aname, string oldpass, string newpass)
        {
            //先取得该用户的随机密钥
            Admin admin = getAdminByAname(aname);
            if (admin == null || string.IsNullOrEmpty(admin.Asalt))
            {
                return 0;
            }
            //把明文进行加密重新赋值
            oldpass = Sunny.Common.DESEncrypt.Encrypt(oldpass, admin.Asalt);
            newpass = Sunny.Common.DESEncrypt.Encrypt(newpass, admin.Asalt);
            if (admin.Apass == oldpass)
            {
                admin.Apass = newpass;
                //be.ObjectStateManager.ChangeObjectState(admin, EntityState.Modified);
                return we.SaveChanges();
            }
            else
            {
                return 0;
            }
        }

        public Admin login(String aname, String apass)
        {
            //先取得该用户的随机密钥
            Admin admin = getAdminByAname(aname);
            if (admin == null || string.IsNullOrEmpty(admin.Asalt))
            {
                return null;
            }
            //把明文进行加密重新赋值
            apass = Sunny.Common.DESEncrypt.Encrypt(apass, admin.Asalt);
            return we.Admin.SingleOrDefault(a => a.Aname == aname && a.Apass == apass);
        }

    }
}