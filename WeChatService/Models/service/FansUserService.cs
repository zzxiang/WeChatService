﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WeChatService.Models.efmodel;

namespace WeChatService.Models.service
{
    public class FansUserService : ServiceBase<FansUser>
    {
        public FansUser GetByOpenID(String openid)
        {
            return we.FansUser.SingleOrDefault(j => j.OpenID == openid);
        }

        public int Del(String openid)
        {
            FansUser fu = GetByOpenID(openid);
            if (fu!=null)
            {
                return Del(fu);
            }
            return 0;
        }
    }
}