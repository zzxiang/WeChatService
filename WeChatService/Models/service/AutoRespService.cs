﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WeChatService.Models.efmodel;
using Webdiyer.WebControls.Mvc;

namespace WeChatService.Models.service
{
    public class AutoRespService:ServiceBase<AutoResp>
    {
        public PagedList<AutoResp> getPage(string serkey, int pi, int pz, int eventType)
        {
            if (String.IsNullOrEmpty(serkey))
            {
                return we.AutoResp.Where(j => j.Event == eventType)
                      .OrderByDescending(j => j.ID)
                      .ToPagedList(pi, pz);
            }
            return we.AutoResp.Where(j => j.Event == eventType && j.Tags.Contains(serkey))
                      .OrderByDescending(j => j.ID)
                      .ToPagedList(pi, pz);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="e">1 默认回复  2 关注回复  3 关键词回复</param>
        /// <returns></returns>
        public int GetCountByEvent(int e)
        {
            return we.AutoResp.Count(j => j.Event == e);
        }


        public int AddList(List<AutoResp> list)
        {
            foreach (var autoResp in list)
            {
                we.AutoResp.AddObject(autoResp);
            }
            return we.SaveChanges();
        }


        public AutoResp GetByID(int id)
        {
            return we.AutoResp.SingleOrDefault(j => j.ID == id);
        }


        public AutoResp GetByTag(String tag)
        {
            return we.AutoResp.SingleOrDefault(j => j.Tags == tag);
        }

        public int Del(int id)
        {
            return Del(GetByID(id));
        }

        /// <summary>
        /// 检查消息数组 是否已经存在 ，存在返回位置，否则 返回 -1
        /// </summary>
        /// <param name="tags"></param>
        /// <returns></returns>
        public int CheckTagExist(String[] tags)
        {
            for (int i = 0; i < tags.Count(); i++)
            {
                if (String.IsNullOrEmpty(tags[i]))
                {
                    continue;
                }
                AutoResp ar = GetByTag(tags[i]);
                if (ar != null)
                {
                    return i;
                }
            }
            return -1;
        }


        public AutoResp GetByKeyOrEvent(int eType, String msg)
        {
            if (eType == Common.EventType_Follow)
            {
                AutoResp a = we.AutoResp.SingleOrDefault(j => j.Event == 2);
                if (a != null)
                {
                    return a;
                }
            }
            else if (eType == Common.EventType_Key)
            {
                AutoResp a = GetByTag(msg);
                if (a != null)
                {
                    return a;
                }
            }
            else if (eType == Common.EventType_Other)
            {
                AutoResp a = we.AutoResp.SingleOrDefault(j => j.Event == 1);
                if (a != null)
                {
                    return a;
                }
            }

            return null;
        }
    }
}