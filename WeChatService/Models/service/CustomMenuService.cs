﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WeChatService.Models.efmodel;

namespace WeChatService.Models.service
{
    public class CustomMenuService : ServiceBase<CustomMenu>
    {
        public CustomMenu getByID(int ID)
        {
            return we.CustomMenu.SingleOrDefault(t => t.ID == ID);
        }

        public List<CustomMenu> getListByRootID(int RootID)
        {
            return we.CustomMenu.Where(t => t.RootID == RootID).ToList();
        }

        public int Edit(CustomMenu c)
        {
            CustomMenu item = getByID(c.ID);
            item.Title = c.Title;
            item.Key = c.Key;
            item.RootID = c.RootID;
            item.Url = c.Url;
            item.Type = c.Type;
            return we.SaveChanges();
        }

        /// <summary>
        /// 删除菜单及子菜单
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public int Del(int ID)
        {
            CustomMenu item = getByID(ID);
            if (item == null)
            {
                return 0;
            }
            List<CustomMenu> list = getListByRootID(item.ID);
            DelList(list);
            return Del(item);
        }
    }
}