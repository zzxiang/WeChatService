﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WeChatService.Models.efmodel;

namespace WeChatService.Models.service
{
    public class GroupService : ServiceBase<Group>
    {
        public Group GetByName(String name)
        {
            return we.Group.SingleOrDefault(j => j.Name == name);
        }
    }
}