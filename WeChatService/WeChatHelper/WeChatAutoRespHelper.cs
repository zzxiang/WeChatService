﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sunny.Common;
using WX.Framework;
using WeChatService.Models;
using WeChatService.Models.efmodel;
using WeChatService.Models.service;
using WeChatService.WeChatHelper.Handlers;
using WX.Model;

namespace WeChatService.WeChatHelper
{
    public class WeChatAutoRespHelper
    {

        /// <summary>
        /// 1.关注 2.文字消息 3.其他 4.取消关注 
        /// 次序跟 WeChatService.Models.Common 一 一对应，不可随意更改。
        /// </summary>
        private int Type;

        /// <summary>
        /// 接收到的用户发来的文字信息
        /// </summary>
        private String Msg;

        public WeChatAutoRespHelper(int Type, String Msg)
        {
            this.Type = Type;
            this.Msg = Msg;
        }

        public IMessageHandler GetMessageHandler()
        {
            MessageService messageService = new MessageService();
            AutoRespService autoRespService = new AutoRespService();

            AutoResp autoResp = autoRespService.GetByKeyOrEvent(Type, Msg);

            if (autoResp != null)
            {
                if (autoResp.Type == Common.MsgType_Txt)
                {
                    int msgId = int.Parse(autoResp.MsgID.Trim(';'));
                    Message message = messageService.getByID(msgId);
                    if (message != null)
                    {
                        return new WeChatMessageTxtHandler(message.Text);
                    }

                }
                else if (autoResp.Type == Common.MsgType_Music)
                {
                    int msgId = int.Parse(autoResp.MsgID.Trim(';'));
                    Message message = messageService.getByID(msgId);
                    if (message != null)
                    {
                        MusicMessage mm = new MusicMessage()
                        {
                            Description = message.Description,
                            HQMusicUrl = message.MusicURL,
                            MusicURL = message.MusicURL,
                            Title = message.Title
                        };
                        return new WeChatMessageMusicHandler(mm);
                    }
                }
                else if (autoResp.Type == Common.MsgType_PictureTxt
                         || autoResp.Type == Common.MsgType_PictureTxts)
                {
                    List<Message> msgList = messageService.getListByMids(autoResp.MsgID);
                    if (msgList != null && msgList.Any())
                    {
                        List<ArticleMessage> list = GetByMessageList(msgList);
                        return new WeChatMessagePictureTxtsHandler(list);
                    }
                }
            }

            String Text = "亲，我还无法了解您的需求，我会不断改进的！";

            return new WeChatMessageTxtHandler(Text);
        }

        private List<ArticleMessage> GetByMessageList(List<Message> list)
        {
            List<ArticleMessage> amList = new List<ArticleMessage>();
            int iFlag = 0;
            foreach (var message in list)
            {
                ArticleMessage articleMessage = new ArticleMessage();
                articleMessage.Title = message.Title;
                articleMessage.Description = message.Description;
                articleMessage.Url = Common.HTTP + Common.thisurl + "/" + Common.ContentUrl +
                                     Utils.ToBase64Str(message.ID.ToString());
                if (iFlag == 0)
                {
                    articleMessage.PicUrl = Common.HTTP + Common.thisurl + "/" + Common.CoverImageUrl_Big + message.ImgUrl;
                }
                else
                {
                    articleMessage.PicUrl = Common.HTTP + Common.thisurl + "/" + Common.CoverImageUrl_Small + message.ImgUrl;
                }

                amList.Add(articleMessage);
                iFlag++ ;
            }
            return amList;
        }
    }
}