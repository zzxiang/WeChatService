﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WX.Api;
using WX.Framework;
using WX.Model;
using WX.Model.ApiRequests;
using WeChatService.Models;
using WeChatService.Models.efmodel;
using WeChatService.Models.service;
using Group = WX.Model.Group;

namespace WeChatService.WeChatHelper
{
    public class WeChatEventHelper
    {

        /// <summary>
        /// 将用户移到默认分组并保存到数据库 
        /// </summary>
        /// <param name="msg"></param>
        public void Event_Follow(MiddleMessage msg)
        {
            String openid = msg.RequestMessage.FromUserName;
            DefaultGroupCreate();
            GroupMemberUpdate(openid, Common.DefaultGroupName);
        }

        /// <summary>
        /// 将用户从数据库 删除
        /// </summary>
        /// <param name="msg"></param>
        public void Event_UnFollow(MiddleMessage msg)
        {
            String openid = msg.RequestMessage.FromUserName;
            FansUserService fus = new FansUserService();
            fus.Del(openid);
        }

        /// <summary>
        /// 创建默认分组
        /// </summary>
        public void DefaultGroupCreate()
        {
            var groupService = new GroupService();
            WeChatService.Models.efmodel.Group group = groupService.GetByName(Common.DefaultGroupName);
            if (group == null)
            {
                var request = new GroupsCreateRequest()
                {
                    AccessToken = ApiAccessTokenManager.Instance.GetCurrentToken(),
                    Group = new Group
                    {
                        Name = Common.DefaultGroupName
                    }
                };
                IApiClient client = new DefaultApiClient();
                var response = client.Execute(request);
                if (!response.IsError)
                {
                    Group g = response.Group;
                    var gg = new WeChatService.Models.efmodel.Group()
                    {
                        Name = g.Name,
                        GroupID = g.ID
                    };
                    groupService.Add(gg);
                }
            }
        }

        /// <summary>
        /// 移动用户到分组
        /// </summary>
        /// <param name="openid"></param>
        /// <param name="toGroupName"></param>
        public void GroupMemberUpdate(String openid,String toGroupName)
        {
            var groupService = new GroupService();
            WeChatService.Models.efmodel.Group group = groupService.GetByName(toGroupName);
            if (group != null && group.GroupID != null)
            {
                var request = new GroupsMembersUpdateRequest()
                {
                    AccessToken = ApiAccessTokenManager.Instance.GetCurrentToken(),
                    OpenId = openid,
                    ToGroupId = (int)group.GroupID
                };
                IApiClient client = new DefaultApiClient();
                var response = client.Execute(request);
                if (!response.IsError)
                {
                    FansUser fansUser = new FansUser();
                    fansUser.OpenID = openid;
                    fansUser.CreateTime = DateTime.Now;
                    FansUserService fus = new FansUserService();
                    fus.Add(fansUser);
                }
            }
        }
    }
}