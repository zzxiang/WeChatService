﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WX.Api;
using WX.Framework;
using WX.Model;
using WX.Model.ApiRequests;
using WeChatService.Models;
using WeChatService.Models.efmodel;
using WeChatService.Models.service;

namespace WeChatService.Helper
{
    public class WeChatCustomMenuHelper
    {
        public String SetCustomMenuToWX()
        {
            CustomMenuService customMenuService = new CustomMenuService();
            List<CustomMenu> list = customMenuService.GetAll();
            if (list == null || !list.Any())
            {
                return Common.MenuCreateError_1;
            }
            
            IApiClient client = new DefaultApiClient();
            var request = new MenuCreateRequest()
            {
                AccessToken = ApiAccessTokenManager.Instance.GetCurrentToken(),
                Buttons = BuildButton(list)
            };

            var response = client.Execute(request);
            if (response.IsError)
            {
                return response.ToString();
            }
            else
            {
                return Common.MenuCreateSuccess;
            }
        }

        public String DelCustomMenuFromWX()
        {
            IApiClient client = new DefaultApiClient();
            var request = new MenuDeleteRequest()
            {
                AccessToken = ApiAccessTokenManager.Instance.GetCurrentToken()
            };

            var response = client.Execute(request);
            if (response.IsError)
            {
                return response.ToString();
            }
            else
            {
                return Common.MenuDeleteSuccess;
            }
        }

        private IEnumerable<ClickButton> BuildButton(List<CustomMenu> list)
        {
            var result = new List<ClickButton>();
            foreach (var r in list.Where(r => r.RootID==0))
            {
                ClickButton button;
                var subButton = list.Where(s => s.RootID == r.ID);
                if (subButton.Any())
                {
                    var listClickButton = new List<ClickButton>();
                    int ia = 0;
                    foreach (var s in subButton)
                    {
                        ClickButton cb = new ClickButton
                        {
                            Name = s.Title,
                            Key = s.Key,
                            Type = s.Type == "View" ? ClickButtonType.view : ClickButtonType.click,
                            Url = s.Type == "View" ? s.Url : ""
                        };
                        if (ia < 5)
                        {
                            listClickButton.Add(cb);
                        }
                        ia++;
                    }
                    button = new ClickButton
                    {
                        Name = r.Title,
                        SubButton = listClickButton
                    };
                }
                else
                {
                    button = new ClickButton
                    {
                        Name = r.Title,
                        Key = r.Key,
                        Type = r.Type == "View" ? ClickButtonType.view : ClickButtonType.click
                    };
                }
                if (button.Type == ClickButtonType.view)
                {
                    button.Url = r.Url;
                }
                result.Add(button);
            }
            return result.Take(3);
        }
    }
}