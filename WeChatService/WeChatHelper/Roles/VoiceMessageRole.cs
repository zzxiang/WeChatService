﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WeChatHelper.Handlers;
using WX.Framework;
using WX.Model;

namespace WeChatHelper.Roles
{
    public class VoiceMessageRole : IMessageRole
    {
        public IMessageHandler MessageRole(MiddleMessage message)
        {
            var request = message.RequestMessage as RequestVoiceMessage;
            if (request != null)
            {
                return new VoiceMessageHandler();
            }
            else
            {
                return new DefaultMessageHandler();
            }
        }
    }
}
