﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using WX.Framework;
using WX.Model;
using WeChatService.Models;
using WeChatService.WeChatHelper;

namespace WeChatHelper
{
    public class TextMessageRole : IMessageRole
    {
        public IMessageHandler MessageRole(MiddleMessage msg)
        {
            var request = (RequestTextMessage)msg.RequestMessage;

            WeChatAutoRespHelper autoRespHelper = new WeChatAutoRespHelper(Common.EventType_Key, request.Content);

            return autoRespHelper.GetMessageHandler();
        }
    }
}