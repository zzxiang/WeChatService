﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using WX.Framework;
using WX.Model;
using WeChatService.Models;
using WeChatService.WeChatHelper;

namespace WeChatHelper
{
    public class EventMessageRole : IMessageRole
    {
        public IMessageHandler MessageRole(MiddleMessage msg)
        {
            var eventType = (Event)Enum.Parse(typeof(Event), msg.Element.Element("Event").Value, true);

            if (eventType == Event.Subscribe)
            {
                WeChatEventHelper weChatEventHelper = new WeChatEventHelper();
                weChatEventHelper.Event_Follow(msg);
                return new WeChatAutoRespHelper(Common.EventType_Follow, null).GetMessageHandler();
            }
            else if (eventType == Event.Unsubscribe)
            {
                WeChatEventHelper weChatEventHelper = new WeChatEventHelper();
                weChatEventHelper.Event_UnFollow(msg);
                return new WeChatAutoRespHelper(Common.EventType_UnFollow, null).GetMessageHandler();
            }
            else if (eventType==Event.Click)
            {
                String clickKey = msg.Element.Element("EventKey").Value;
                return new WeChatAutoRespHelper(Common.EventType_Key, clickKey).GetMessageHandler();
            }
//            else if (eventType==Event.Location)
//            {
//                String latitude = msg.Element.Element("Latitude").Value;
//                String longitude = msg.Element.Element("Longitude").Value;
//                String precision = msg.Element.Element("Precision").Value;
//                String respMsg = "Latitude:" + latitude + "\r Longitude:" + longitude;
//                CommonTextMessageHandler cmh = new CommonTextMessageHandler(respMsg);
//                return cmh;
//            }

            return new WeChatAutoRespHelper(Common.EventType_Other, null).GetMessageHandler();
        }
    }
}