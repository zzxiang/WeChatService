﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using WX.Framework;
using WX.Model;

namespace WeChatHelper
{
    public class SubScribeEventMessageHandler : TextMessageHandler
    {
        private static string subScribeMsg = "欢迎关注本微信，这是一个测试账号，你可以发送 空空、博客 等获取更多内容~";

        public SubScribeEventMessageHandler()
            : base(subScribeMsg)
        {
        }
    }
}
