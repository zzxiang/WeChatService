﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WX.Framework;
using WX.Model;

namespace WeChatService.WeChatHelper.Handlers
{
    public class WeChatMessageMusicHandler : IMessageHandler
    {
        private MusicMessage musicMessage;

        public WeChatMessageMusicHandler(MusicMessage musicMessage)
        {
            this.musicMessage = musicMessage;
        }

        public ResponseMessage HandlerRequestMessage(MiddleMessage message)
        {
            return new ResponseMusicMessage(message.RequestMessage)
            {
                CreateTime = DateTime.Now.Ticks,
                Music = new MusicMessage
                {
                    Description = musicMessage.Description,
                    HQMusicUrl = musicMessage.HQMusicUrl,
                    MusicURL = musicMessage.MusicURL,
                    ThumbMediaId = musicMessage.ThumbMediaId ?? "",
                    Title = musicMessage.Title
                }
            };
        }
    }
}