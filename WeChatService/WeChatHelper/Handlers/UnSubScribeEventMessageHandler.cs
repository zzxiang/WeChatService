﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using WX.Framework;
using WX.Model;

namespace WeChatHelper
{
    public class UnSubScribeEventMessageHandler : TextMessageHandler
    {
        private static string unsubScribeMsg = "让您失望了，任何意见或建议请联系 QQ：357112618。";

        public UnSubScribeEventMessageHandler()
            : base(unsubScribeMsg)
        {
        }
    }
}
