﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WX.Framework;
using WX.Model;

namespace WeChatHelper
{
    public class MusicMessageHandler : IMessageHandler
    {
        public ResponseMessage HandlerRequestMessage(MiddleMessage message)
        {
            return new ResponseMusicMessage(message.RequestMessage)
                {
                    CreateTime = DateTime.Now.Ticks,
                    Music = new MusicMessage
                        {
                            Description = "情之所到处，我所做的一切加你所做的一切，身不由己后，我没能做的一切加你没能做的一切，就是浪漫。",
                            HQMusicUrl = "http://down.qnwz.cn/uploads/media/broadcast/storymagazine/%E4%BB%80%E4%B9%88%E6%98%AF%E6%B5%AA%E6%BC%AB.mp3",
                            MusicURL = "http://down.qnwz.cn/uploads/media/broadcast/storymagazine/%E4%BB%80%E4%B9%88%E6%98%AF%E6%B5%AA%E6%BC%AB.mp3",
                            ThumbMediaId = "OnQLWP18mC68RZu4HKYfUJqFkQWBFP-TW30UrazTq0voPHTrE49lOrQOyRxtUtrG",
                            Title = "什么是浪漫"
                        }
                };
        }
    }
}
