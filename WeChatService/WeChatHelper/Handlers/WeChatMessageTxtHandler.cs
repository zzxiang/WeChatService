﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WX.Framework;
using WX.Model;

namespace WeChatService.WeChatHelper.Handlers
{
    public class WeChatMessageTxtHandler : IMessageHandler
    {
        private string respMsg;

        public WeChatMessageTxtHandler(String respMsg)
        {
            this.respMsg = respMsg;
        }

        public ResponseMessage HandlerRequestMessage(MiddleMessage message)
        {
            return new ResponseTextMessage(message.RequestMessage)
            {
                CreateTime = DateTime.Now.Ticks,
                Content = respMsg
            };
        }
    }
}