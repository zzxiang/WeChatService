﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WX.Framework;
using WX.Model;

namespace WeChatHelper
{
    public class CommonTextMessageHandler : IMessageHandler
    {
        private string respMsg;

        public CommonTextMessageHandler(String respMsg)
        {
            this.respMsg = respMsg;
        }

        public ResponseMessage HandlerRequestMessage(MiddleMessage message)
        {
            return new ResponseTextMessage(message.RequestMessage)
            {
                CreateTime = DateTime.Now.Ticks,
                Content = respMsg
            };
        }
    }
}
