﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using WX.Framework;
using WX.Model;

namespace WeChatHelper
{
    public class CnblogsTextMessageHandler : IMessageHandler
    {
        private static string s_cnblogsMsg = 
            "HI，欢迎来到空空的微信世界~";
        public ResponseMessage HandlerRequestMessage(MiddleMessage msg)
        {
            
            return new ResponseTextMessage(msg.RequestMessage)
            {
                CreateTime = DateTime.Now.Ticks,
                Content = s_cnblogsMsg
            };
        }
    }
}