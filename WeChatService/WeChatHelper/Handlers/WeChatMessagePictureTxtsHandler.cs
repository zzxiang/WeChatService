﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WX.Framework;
using WX.Model;

namespace WeChatService.WeChatHelper.Handlers
{
    public class WeChatMessagePictureTxtsHandler:IMessageHandler
    {
        private List<ArticleMessage> articleMessages;

        public WeChatMessagePictureTxtsHandler(List<ArticleMessage> articleMessages)
        {
            this.articleMessages = articleMessages;
        }

        public ResponseMessage HandlerRequestMessage(MiddleMessage msg)
        {
            var response = new ResponseNewsMessage(msg.RequestMessage);
            response.ArticleCount = articleMessages.Count;
            response.CreateTime = DateTime.Now.Ticks;
            response.Articles = articleMessages;
            return response;
        }
    }
}