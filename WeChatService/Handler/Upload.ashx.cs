﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Web;
using Sunny.Common;
using WeChatService.Models;

namespace Web
{
    /// <summary>
    /// jquery.uploadify  上传文件处理类
    /// </summary>
    public class Upload : IHttpHandler
    {
        public void ProcessRequest(HttpContext context)
        {
            String t = context.Request["t"];

            HttpPostedFile file = context.Request.Files["FileData"];
            if (file == null)
            {
                context.Response.Write("0|");
                return;
            }
            if (t=="coverimg")
            {
                String filename = DateTime.Now.ToFileTime() + new Random().Next(999) + "." + Utils.GetFileExt(file.FileName);
                string uploadpath = context.Server.MapPath("~/Upload/CoverImage");
                String filepath = uploadpath + "/" + filename;//保存原图路径
                String slfilepath = uploadpath + "/S400/" + filename;

                if (!Directory.Exists(uploadpath))
                {
                    Directory.CreateDirectory(uploadpath);
                }
                file.SaveAs(filepath);
                //生成缩略图
                FileStream fs = new FileStream(filepath, FileMode.Open);
                Imager.CutForSquare(fs, slfilepath, 400, 90);
                context.Response.Write("1|" + filename); //标志位1标识上传成功，后面的可以返回前台的参数，比如上传后的路径等，中间使用|隔开
           
            }else if (t == "msc")
            {
                String filename = DateTime.Now.ToFileTime() + new Random().Next(999) + "." + Utils.GetFileExt(file.FileName);
                string uploadpath = context.Server.MapPath("~/Upload/Music");
                String filepath = uploadpath + "/" + filename;//保存原图路径
                
                if (!Directory.Exists(uploadpath))
                {
                    Directory.CreateDirectory(uploadpath);
                }
                file.SaveAs(filepath);
                String musicurl = Common.HTTP + Common.thisurl + "/Upload/Music/" + filename;
                context.Response.Write("1|" + musicurl); //标志位1标识上传成功，后面的可以返回前台的参数，比如上传后的路径等，中间使用|隔开
           
            }


        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}