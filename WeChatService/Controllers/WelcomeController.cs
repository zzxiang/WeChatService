﻿using System.Web.Mvc;
using WeChatService.Controllers.Filter;
using WeChatService.Models.efmodel;
using WeChatService.Models.service;

namespace WeChatService.Controllers
{
    public class WelcomeController : Controller
    {
        //
        // GET: /Welcome/
        [LoginFilter]
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(string Aname, string Apass)
        {
            AdminService service = new AdminService();
            Admin a = service.login(Aname, Apass);
            if (a==null)
            {
                ViewBag.msg = "用户名或密码有误，请重试！";
                return View();
            }
            else
            {
                Session["admin"] = a;
                Session.Timeout = 45;
                ViewBag.msg = "ok";
                return View();
            }
        }

    }
}
