﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Sunny.Common;
using WeChatService.Models.efmodel;
using WeChatService.Models.service;

namespace WeChatService.Controllers
{
    public class ContentController : Controller
    {
        MessageService ms = new MessageService();

        /// <summary>
        /// 用于微信显示的详情页面
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        public ActionResult Index(String p)
        {
            try
            {
                //int msgId = int.Parse(p);
                int msgId = int.Parse(Utils.DeBase64Str(p));
                Message msg = ms.getByID(msgId);
                return View(msg);
            }
            catch (Exception)
            {
                return null;
            }

        }

    }
}
