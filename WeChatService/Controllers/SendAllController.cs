﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WeChatService.Models;
using WeChatService.Models.service;

namespace WeChatService.Controllers
{
    public class SendAllController : BaseController
    {
        SendAllService sendAllService = new SendAllService();

        public ActionResult Index(int pageIndex = 1)
        {

            var page = sendAllService.getPage(pageIndex, Common.backPageSize);
            return View(page);
        }

    }
}
