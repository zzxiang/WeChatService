﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Sunny.Common;
using WeChatService.Models;
using WeChatService.Models.efmodel;
using WeChatService.Models.service;

namespace WeChatService.Controllers
{
    public class AutoRespController : BaseController
    {
        AutoRespService ars = new AutoRespService();

        public ActionResult Follow(int pageIndex = 1, String serkey = "")
        {
            ViewBag.serkey = serkey;
            var page = ars.getPage(serkey, pageIndex, Common.backPageSize, 2);
            return View(page);
        }

        public ActionResult Key(int pageIndex = 1, String serkey = "")
        {
            ViewBag.serkey = serkey;
            var page = ars.getPage(serkey, pageIndex, Common.backPageSize, 3);
            return View(page);
        }

        public ActionResult Default(int pageIndex = 1, String serkey = "")
        {
            ViewBag.serkey = serkey;
            var page = ars.getPage(serkey, pageIndex, Common.backPageSize, 1);
            return View(page);
        }

        [HttpPost]
        public ActionResult Add(String eventType, String key, String type, String msgid)
        {
            int Ev = int.Parse(eventType);
            int Type = int.Parse(type);
            String[] tags = key.Split('#');

            //如果是关注、默认，只能存在一条
            if (Ev==1||Ev==2)
            {
                int count = ars.GetCountByEvent(Ev);
                if (count>0)
                {
                    return Json(new { rel = 0, msg = Common.ExistMsg });
                }
            }
            //如果1 文字  2 音乐  3 图文，msgid 不允许存在多个
            if ((Type==1||Type==2||Type==3)&&msgid.Contains(";"))
            {
                return Json(new { rel = 0, msg = Common.Error_1 });
            }
            if (tags.Count() > 0)
            {
                int index = ars.CheckTagExist(tags);
                if (index > -1)
                {
                    return Json(new { rel = 0, msg = tags[index] + Common.ExistMsg });
                }
                List<String> noSameTags = Utils.DelSame(tags);
                List<AutoResp> list = new List<AutoResp>();
                foreach (var tag in noSameTags)
                {
                    if (String.IsNullOrEmpty(tag))
                    {
                        continue;
                    }
                    AutoResp ar = new AutoResp();
                    ar.Event = Ev;
                    ar.Tags = tag;
                    ar.Type = Type;
                    ar.MsgID = msgid.Trim(';');
                    list.Add(ar);
                }
                int irel = ars.AddList(list) > 0 ? 1 : 0;
                return Json(new { rel = irel, msg = irel == 1 ? Common.SuccessMsg : Common.FailedMsg });
            }
            else
            {
                AutoResp ar = new AutoResp();
                ar.Event = Ev;
                ar.Tags = key;
                ar.Type = Type;
                ar.MsgID = msgid.Trim(';');
                int irel = ars.Add(ar) > 0 ? 1 : 0;
                return Json(new { rel = irel, msg = irel == 1 ? Common.SuccessMsg : Common.FailedMsg });
            }
        }

        [HttpPost]
        public ActionResult Del(String id)
        {
            int ID = int.Parse(id);
            int irel = ars.Del(ID) > 0 ? 1 : 0;
            return Json(new { rel = irel, msg = irel == 1 ? Common.SuccessMsg : Common.FailedMsg });
        }
    }
}
