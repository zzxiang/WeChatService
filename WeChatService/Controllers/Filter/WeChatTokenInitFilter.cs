﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WX.Framework;
using WeChatService.Models;

namespace WeChatService.Controllers.Filter
{
    public class WeChatTokenInitFilter : FilterAttribute, IActionFilter
    {

        public void OnActionExecuting(ActionExecutingContext filterContext)
        {
            
        }

        public void OnActionExecuted(ActionExecutedContext filterContext)
        {
            
        }
    }
}
