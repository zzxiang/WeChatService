﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WeChatService.Controllers.Filter
{
    public class LoginFilter : FilterAttribute, IActionFilter
    {
        public void OnActionExecuted(ActionExecutedContext filterContext)
        {
            
        }

        public void OnActionExecuting(ActionExecutingContext filterContext)
        {
            //拦截器取session，判断是否登录
            object o = filterContext.HttpContext.Session["admin"];
            if (o == null)
            {
                //未登录则跳转到指定页面
                RedirectResult rr = new RedirectResult(WeChatService.Models.Common.loginpage);
                filterContext.Result = rr;
                rr.ExecuteResult(filterContext);
            }
            //已登录，不做任何处理
        }
    }
}