﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Sunny.Common;
using WeChatService.Models;
using WeChatService.Models.service;
using WeChatService.Models.efmodel;

namespace WeChatService.Controllers
{
    public class MessageController : BaseController
    {
        MessageService ms = new MessageService();

        public ActionResult Index(int pageIndex = 1, String serkey = "", String type = "")
        {
            ViewBag.serkey = serkey;
            var page = ms.getPage(serkey, pageIndex, Common.backPageSize, type);
            return View(page);
        }

        public ActionResult Simple(int pageIndex = 1, String serkey = "", String type = "")
        {
            ViewBag.serkey = serkey;
            ViewBag.type = type;
            if (type == "4") type = "2";
            else if (type == "3") type = "2";
            else if (type == "2") type = "3";
            
            var page = ms.getPage(serkey, pageIndex, Common.backPageSize, type);
            return View(page);
        }

        [HttpPost]
        public ActionResult AddTxtMsg(String txt)
        {
            Message message = new Message();
            message.Title = txt.Substring(0,txt.Length>15?14:txt.Length);
            message.Text = txt;
            message.Type = "1";
            message.CreateTime = DateTime.Now;
            int irel = ms.Add(message) > 0 ? 1 : 0;
            return Json(new { rel = irel, msg = irel == 1 ? Common.SuccessMsg : Common.FailedMsg });
        }

        [HttpPost]
        public ActionResult EditTxtMsg(String id,String txt)
        {
            int ID = int.Parse(id);
            Message message = new Message();
            message.Title = txt.Substring(0, txt.Length > 15 ? 14 : txt.Length);
            message.ID = ID;
            message.Text = txt;
            int irel = ms.Edit(message) > 0 ? 1 : 0;
            return Json(new { rel = irel, msg = irel == 1 ? Common.SuccessMsg : Common.FailedMsg });
        }

        [HttpPost]
        public ActionResult AddMscMsg(String title,String desp,String musicurl)
        {
            Message message = new Message();
            message.Title = title;
            message.Text = "";
            message.Description = desp;
            message.MusicURL = musicurl;
            message.Type = "3";
            message.CreateTime = DateTime.Now;
            int irel = ms.Add(message) > 0 ? 1 : 0;
            return Json(new { rel = irel, msg = irel == 1 ? Common.SuccessMsg : Common.FailedMsg });
        }

        [HttpPost]
        public ActionResult EditMscMsg(String id,String title, String desp, String musicurl)
        {
            int ID = int.Parse(id);
            Message message = new Message();
            message.ID = ID;
            message.Title = title;
            message.Description = desp;
            message.MusicURL = musicurl;
            int irel = ms.Edit(message) > 0 ? 1 : 0;
            return Json(new { rel = irel, msg = irel == 1 ? Common.SuccessMsg : Common.FailedMsg });
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult AddPttMsg()
        {
            String title = Request["title"],
                   image = Request["image"],
                   text = Request["text"];
            Message message = new Message();
            message.Title = title;
            message.ImgUrl = image;
            message.Text = text;
            message.Type = "2";
            message.CreateTime = DateTime.Now;
            int irel = ms.Add(message) > 0 ? 1 : 0;
            return Json(new { rel = irel, msg = irel == 1 ? Common.SuccessMsg : Common.FailedMsg });
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult EditPttMsg()
        {
            String title = Request["title"],
                   image = Request["image"],
                   text = Request["text"],
                   id = Request["id"];
            int ID = int.Parse(id);
            Message message = new Message();
            message.ID = ID;
            message.Title = title;
            message.ImgUrl = image;
            message.Text = text;
            int irel = ms.Edit(message) > 0 ? 1 : 0;
            return Json(new { rel = irel, msg = irel == 1 ? Common.SuccessMsg : Common.FailedMsg });
        }

        [HttpPost]
        public ActionResult Del(String id)
        {
            int ID = int.Parse(id);
            int irel = ms.Del(ID) > 0 ? 1 : 0;
            return Json(new { rel = irel, msg = irel == 1 ? Common.SuccessMsg : Common.FailedMsg });
        }

        [HttpPost]
        public ActionResult GetMsgByID(String id)
        {
            int ID = int.Parse(id);
            Message msg = ms.getByID(ID);
            return Json(msg);
        }


        
    }
}
