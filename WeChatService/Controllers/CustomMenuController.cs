﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WeChatService.Controllers.Filter;
using WeChatService.Helper;
using WeChatService.Models.efmodel;
using WeChatService.Models.service;
using WeChatService.Models;

namespace WeChatService.Controllers
{
    public class CustomMenuController : BaseController
    {
        CustomMenuService cms = new CustomMenuService();
        //
        // GET: /CustomMenu/

        public ActionResult Index()
        {
            List<CustomMenu> list = cms.GetAll();
            return View(list);
        }

        [HttpPost]
        public ActionResult Add(String title, String key, String link, String root, String type)
        {
            int rootID = int.Parse(root);
            if (rootID == 0)
            {
                if (checkFatherMenuFull())
                {
                    return Json(new {rel = -1, msg = Common.CustomMenuCreateErrorMsg_1});
                }
            }
            else
            {
                if (checkSonMenuFull(rootID))
                {
                    return Json(new { rel = -2, msg = Common.CustomMenuCreateErrorMsg_2 });
                }
            }
            var cm = new CustomMenu()
            {
                Title = title,
                Key = key,
                RootID = rootID,
                Type = type,
                Url = link
            };
            int irel = cms.Add(cm) > 0 ? 1 : 0;
            return Json(new {rel = irel, msg = irel == 1 ? Common.SuccessMsg : Common.FailedMsg});
        }

        [HttpPost]
        public ActionResult Edit(String id, String title, String key, String link, String root, String type)
        {
            int rootID = int.Parse(root);
            int ID = int.Parse(id);
            CustomMenu nowMenu = cms.getByID(ID);
            if (rootID == 0)
            {
                if (nowMenu!=null&&nowMenu.RootID!=0&&checkFatherMenuFull())
                {
                    return Json(new { rel = -1, msg = Common.CustomMenuCreateErrorMsg_1 });
                }
            }
            else
            {
                if (nowMenu != null && nowMenu.RootID == 0 && checkSonMenuFull(rootID))
                {
                    return Json(new { rel = -2, msg = Common.CustomMenuCreateErrorMsg_2 });
                }
            }

            
            CustomMenu cm = new CustomMenu()
            {
                ID = ID,
                Title = title,
                Key = key,
                RootID = rootID,
                Type = type,
                Url = link
            };
            int irel = cms.Edit(cm) > 0 ? 1 : 0;
            return Json(new { rel = irel, msg = irel == 1 ? Common.SuccessMsg : Common.FailedMsg });
        }

        [HttpPost]
        public ActionResult Del(String id)
        {
            int ID = int.Parse(id);
            int irel = cms.Del(ID) > 0 ? 1 : 0;
            return Json(new { rel = irel, msg = irel == 1 ? Common.SuccessMsg : Common.FailedMsg });
        }

        [WeChatTokenInitFilter]
        public ActionResult CreateMenu()
        {
            WeChatCustomMenuHelper weChatCustomMenuHelper = new WeChatCustomMenuHelper();
            return Json( new { msg = weChatCustomMenuHelper.SetCustomMenuToWX()});
        }

        [WeChatTokenInitFilter]
        public ActionResult DeleteMenu()
        {
            WeChatCustomMenuHelper weChatCustomMenuHelper = new WeChatCustomMenuHelper();
            return Json(new { msg = weChatCustomMenuHelper.DelCustomMenuFromWX() });
        }

        //检查父菜单数量是否已经到达上限
        private bool checkFatherMenuFull()
        {
            List<CustomMenu> listAll = cms.GetAll();
                int rootCount = listAll.Count(t => t.RootID == 0);
            if (rootCount >= 3)
            {
                return true;
            }
            return false;
        }

        //检查子菜单数量
        private bool checkSonMenuFull(int rootid)
        {
            List<CustomMenu> list = cms.getListByRootID(rootid);
            if (list != null && list.Count >= 5)
            {
                return true;
            }
            return false;
        }
    }
}
